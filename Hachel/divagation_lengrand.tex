\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{geometry}
\usepackage{fourier}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage[french]{babel}
\usepackage{url}
\usepackage{graphicx}
\usepackage{color}

\usepackage[%
    all,
    defaultlines=3 % nombre minimum de lignes
]{nowidow}


\parskip12pt
%\parindent0pt

\title{Einstein et les divagations d'un médecin de campagne\thanks{en l'honneur du docteur Destouches, dit
\emph{Céline}, grand écrivain qui a perdu, hélas, les pédales.}}
\author{Jean-Pierre \textsc{Messager} }

\date{\today} 
% \date{\today} date coulde be today 
% \date{25.12.00} or be a certain date
% \date{ } or there is no date 
\begin{document}
% Hint: \title{what ever}, \author{who care} and \date{when ever} could stand 
% before or after the \begin{document} command 
% BUT the \maketitle command MUST come AFTER the \begin{document} command! 
\maketitle


\begin{abstract}
	Comment la prétention à réinventer la Relativité d'Einstein sans l'étudier
	et une fatuité sans bornes conduit un médecin à poster des sottises sur
	l'Internet pendant des décennies.
\end{abstract}

\section{Introduction}

Entre le discours scientifique et les pseudo-sciences il y a un espace
qu'Alexandre \textsc{Moatti}\footnote{\emph{Altersciences, postures, dogmes,
idéologies}, éditions Odile \textsc{Jacob}, 2013} appelle l'\emph{alterscience}.
Il s'agit, par exemple, de discours qui se donnent l'ambition de la
rigueur scientifique, qui en reprenne le style, en particulier l'usage
de mathématiques et la confrontation avec l'expérience, mais qui n'en
ont que l'apparence. 

Ces discours ne sont pas aussi faciles à démonter que, par exemple, des
fantaisies comme l'astrologie ou la divination par le marc de café.

L'Internet est une véritable mine d'or\footnote{ou d'autres matières,
selon le point de vue...}
pour l'amateur du phénomène,
d'innombrables sites Web sont référéncés par le site Web
\emph{crank.net}\footnote{\url{https://www.crank.net}, semble
indisponible à la date de rédaction de cet article...}, et il est
très loin d'être exhaustif. 

Les forums de discussion scientifiques sont aussi remplis 
d'interventions de cette nature. Et, parmi eux, les \emph{newsgroups}
Usenet consacrés à la physique\footnote{\emph{sci.physics}, 
\emph{sci.physics.relativity} et \emph{fr.sci.physique}, 
accessibles péniblement à travers \emph{Google Groups} et 
plus confortablement avec un logiciel client NNTP.}, sans
grande suprise.

Un sujet de prédilection des \emph{fantaisistes} (pour traduire
de façon moins désobligeante les terme anglais \emph{crank} ou
\emph{crackpot}\/) est la Relativité Restreinte, introduite
par Einstein en 1905. La raison en est, probablement, que 
cette théorie est à la fois hautement contre-intuitive et
qu'elle se formule en utilisant des mathématiques relativement simples,
du niveau du lycée.

Dans le groupe de discussion \emph{fr.sci.physique} un personnage
s'illustre depuis au moins trente ans pour y proposer régulièrement
sa version personnelle de la Relativité. Il n'est pas physicien
(ce n'est pas le problème en soi, des non-spécialistes peuvent
parfaitement étudier et comprendre la Relativité Restreinte, c'est
d'ailleurs mon cas), mais médecin généraliste dans la région
de Nouvelle-Aquitaine, il poste sous le pseudonyme Richard
\textsc{Hachel}, son véritable nom est Richard \textsc{Lengrand},
je n'hésite pas à le divulguer ici : il n'en fait pas mystère 
et se permet régulièrement de parler du haut de l'autorité qu'il
croit tirer de sa profession, en particulier en matière médicale%
\footnote{Si votre médecin démontrait une dissonnance cognitive
constante sur des sujets scientifiques, préféreriez-vous le savoir
ou non\;?}.

La Relativité n'est pas le seul sujet sur lequel le docteur Lengrand a des certitudes
pour le moins déraisonnables (attentats à New York le 11 septembre 2001, naufrage du
Titanic, pour en citer deux). Le sujet de cet article est d'analyser
et de réfuter plusieurs de ses affirmations concernant
la Relativité. Ma motivation est essentiellement pédagogique%
\footnote{Le fait que le personnage est particulièrement grossier et 
suffisant est une motivation secondaire.}.

Dans le domaine de la Relativité\footnote{ni ailleurs\;!},
le docteur Lengrand n'a fait, apparamment, aucune « victime »,
c'est-à-dire qu'il n'a jamais, semble-t-il, convaincu quiconque, pas
même un autre fantaisiste du même genre, ni sur les groupes francophones,
ni sur les groupes anglophones. Cependant l'exercice permet de mieux
saisir certaines subtilités de la physique en général et de la Relativité
Restreinte en particulier.

\section{La base du « raisonnement » du docteur Lengrand}

Le docteur Lengrand part du principe qu'il faut considérer les événements
physiques se produisant à une certaine distance d'un observateur,
dès lors qu'un signal lumineux est émis à ce moment de cet
endroit-là en direction de l'observateur, comme ayant lieu à
l'instant mesuré par cet observateur quand il reçoit le signal%
\footnote{Le concept n'est pas une invention originale du docteur
Lengrand, elle correspond à ce que l'on visualise sur l'écran
d'un radar -- si aucune correction due au délai de propagation
de l'onde reçue n'est faite --, on l'appelle la \emph{synchronisation du radar}.}.

Dans la suite de cet article j'appellerai cette valeur le 
\emph{« temps apparent »} de l'événement\footnote{Lengrand
s'exprime souvent de façon confuse, et sans grande consistence,
c'est un des seuls éléments de ses interventions que j'ai réussi à
isoler et qui dispose
d'une définition claire. Par ailleurs il parle souvent de 
grandeurs physiques \emph{réelles} ou \emph{observables} sans
jamais définir de quoi il s'agit, impossible donc
d'en parler sérieusement.}
Si on utilise ce « temps » pour calculer d'autre grandeurs
physiques, en particulier des positions, des vitesses, on les
qualifiera d'« apparentes » aussi.

Le sujet principal où Lengrand applique ce concept est le bien
connu \emph{paradoxe des jumeaux} dit de \emph{Langevin} (physicien
français du début du XX\ieme{} siècle).

Le dispositif est le suivant : de deux frères jumeaux, l'un reste
sur Terre, supposée définir un référentiel inertiel et l'autre
voyage vers une étoile distance à une vitesse constante, fait
demi-tour, revient à la même vitesse et retrouve son frère sur la Terre. 
La Relativité prédit que le jumeau voyageur sera plus jeune que sont
frère sédentaire.

On parle de paradoxe parce qu'il peut sembler, au premier abord, que
la situation est symétrique et que l'on pourrait donc en conclure
l'inverse, ce qui serait contradictoire. La résolution du paradoxe
se fonde sur le caractère non-inertiel du trajet du jumeau voyageur%
\footnote{\url{https://math.ucr.edu/home/baez/physics/Relativity/SR/TwinParadox/twin_paradox.html}}.

Le docteur Lengrand prétend qu'il y a un \emph{autre} paradoxe dans ce
scénario : en travaillant avec des vitesses apparentes, lors de
la phase de retour, les valeurs de distances et de temps que l'on
trouve, sur la base de la Relativité, ne vérifient pas la relation :

$$ \textrm{distance} = \textrm{vitesse apparente} \times \textrm{temps propre écoulé} $$

Le temps propre est simplement le temps mesuré par le voyageur 
lors de la phase de retour de son voyage.

Selon le docteur Lengrand, la principale raison de cette prétendue incohérence dans
la Relativité Restreinte d'Einstein est due à la synchronisation d'horloges distantes,
en repos relatif, par une procédure (dite d'Einstein-Poincaré) qu'il
qualifie (sans aucun argument sensible) d'impossible.

\section{Vitesses apparentes et vitesses}

Ce qui m'a frappé, depuis longtemps, est que le docteur Lengrand évite soigneusement
de définir la vitesse apparente. Il prétend soit que c'est évident, soit
qu'il a la même définition que tous les physiciens ou astro-physiciens.
Il fournit, cependant, une formule liant la vitesse $v$ au sens habituel
du terme à la vitesse apparente $v_{\textrm{app}}$ :

$$ v_{\textrm{app}} = \frac{v}{1+\displaystyle\cos\left(\mu\right)\times\frac{v}{c}} $$ 

Le paramètre $\mu$ étant manifestement (le docteur Lengrand ne définit quasiment
jamais rien clairement, mais on a quelques indices que c'est ce qu'il
entend par là) l'angle entre l'axe $Ox$ et le chemin suivi par le jumeau voyageur, il vaut
ici $0$ et puisque $ \cos(0) = 1$, on en arrive à :

$$ v_{\textrm{app}} = \frac{v}{1 + \frac{v}{c}} $$ 

Cette formule reste valable pour $\mu = \pi$ si on convient alors de prendre un $v$
négatif ($ \cos(\pi) = -1 $).
Je vais faire le travail qu'il ne fait pas en démontrant cette formule.
Ceci permettra de savoir précisément sous quelles conditions certaines
propriétés de cette grandeur sont vérifiées ou non.

Considérons un observateur situé à l'origine d'un référentiel, nommée
$O$, donc de coordonnées spatiales $(x, y, z) = (0, 0, 0)$, où se
place le jumeau sédentaire, tandis que son frère voyageur se déplace
à la vitesse $v$ selon l'axe des $x$. Si $v > 0$ il s'éloigne, si 
$v < 0$ il se rapproche. Au temps $t=0$ on suppose qu'il est à la
distance $D$ de la Terre. Pour un événement quelconque, ses coordonnées
dans ce référentiel sont définies selon la procédure décrite dans la
partie I.1 du l'article d'Einstein de 1905%
\footnote{\url{https://www.fourmilab.ch/etexts/einstein/specrel/www/}} 
ou toute autre procédure équivalente. 

Les équations du mouvement du voyageur, dans le référentiel où son frère
est immobile, sont donc :

\begin{eqnarray*}
	x(t) &=& D + vt \\
	y(t) &=& 0 \\
	z(t) &=& 0 
\end{eqnarray*}

On négligera les coordonnées $y$ et $z$ à partir d'ici, car elle sont
toujours nulles, donc inutiles pour les calculs de distances et de
vitesses. On suppose que les horloges des deux jumeaux ont été
initialisées lors du départ à $t = t' = 0$ et sont identiques en
tout point.

Les calculs qui suivent sont tous réalisés dans le référentiel du
jumeau sédentaire, mais sont identiques si on inverse les rôles
dès lors que les mouvements relatifs sont uniformes et qu'on
change $v$ en $-v$.

Le jumeau sédentaire reçoit un premier signal envoyé à un instant 
$t_0$ émis quand le voyageur est à la position $ x_0 = D + vt_0 $.

Le signal est reçu sur la Terre à l'instant :

\begin{eqnarray*}
	T_0 &=& t_0 + \frac{x_0}{c} \\
	    &=& t_0 + \frac{D + vt_0}{c} \\
	    &=& \frac{D}{c} + \left(1 + \frac{v}{c}\right)t_0
\end{eqnarray*}

Plus tard, à l'instant $ t_1 = t_0 + \Delta{}t $, un second signal
est reçu par le jumeau sédentaire, il a donc été émis quand le
voyageur est à la position :

\begin{eqnarray*}
	x_1 &=& D + vt_1 \\
	    &=& D + vt_0 + v\Delta{}t \\
	    &=& D + v\left(t_0 + \Delta{}t\right)
\end{eqnarray*}

On note que :

$$ x_1 - x_0 = v\Delta{}t $$

Ce second signal est reçu sur Terre à l'instant :

\begin{eqnarray*}
	T_1 &=& t_1 + \frac{x_1}{c} \\
	    &=& t_0 + \Delta{}t + \frac{D + v\left(t_0 + \Delta{}t\right)}{c} \\
	    &=& \left(1 + \frac{v}{c}\right)t_0 + \left(1 + \frac{v}{c}\right)\Delta{}t + \frac{D}{c}
\end{eqnarray*}

On peut maintenant calculer la vitesse \emph{apparente}, en divisant la
distance parcourue par le voyageur entre les émissions des deux signaux par l'intervalle
de temps entre leurs réceptions respectives par le jumeau sédentaire qui est :

$$ T_1 - T_0 = \left(1 + \frac{v}{c}\right)\Delta{}t $$

On obtient alors : 

\begin{eqnarray*}
	v_{\textrm{app}} &=& \frac{x_1 - x_0}{T_1 - T_0} \\ 
			 &=& \frac{v\Delta{}t}{\left(1 + \frac{v}{c}\right)\Delta{}t} \\
			 &=& \frac{v}{1 + \frac{v}{c}}
\end{eqnarray*}

C'est la formule publiée d'innombrables fois par le docteur Lengrand. S'il a
une autre façon de la démontrer je serais ravi de la découvrir.
On voit qu'elle est plus faible que $v$ quand le voyageur s'éloigne
et plus grande quand le voyageur se rapproche. On comprend pourquoi les
radaristes doivent prendre en compte ce fait !

On vérifie aussi facilement ceci :

\begin{eqnarray*}
	x_1 - x_0 &=& \frac{v}{1 + \frac{v}{c}} \times \left(1 + \frac{v}{c}\right)\Delta{}t \\
	          &=& \frac{v}{1 + \frac{v}{c}} \times \left(1 + \frac{v}{c}\right)\frac{x_1 - x_0}{v} \\
% 
%	x_1 - x_0 &=& \frac{v}{1 + \frac{v}{c}} \times (T_1 - T_0) \\
%		  &=& \frac{v}{1 + \frac{v}{c}} \times \left(1 + \frac{v}{c}\right)\Delta{}t \\
%		  &=& v_{\textrm{app}} \times \Delta{}t
\end{eqnarray*}

C'est-à-dire :

$$ \textrm{distance parcourue} = \textrm{vitesse apparente} \times \textrm{temps propre écoulé} $$

Il est important de noter les points suivants :

\begin{quote}
	\emph{%
	La situation est symétrique, sur tout intervalle où les jumeaux sont en
	déplacement relatif uniforme, on peut inverser qui envoie un signal à qui,
	et on trouve les mêmes formules.}

	\emph{Entre les deux événements considérés pour évaluer la vitesse apparente, les deux
	jumeaux sont en déplacement uniforme à vitesse constante $v$ l'un par rapport à l'autre,
	en particulier les événements de \emph{réception} des signaux.}

	\emph{L'équation $ x(t) = D + vt $ est supposée vraie pour tout temps propre $t$ de 
	l'observateur, s'il y a un « départ » avant lequel $ x(t) = D $ ou d'arrivée après
	laquelle $ x(t) = 0 $, la relation ci-dessus n'a aucune raison d'être vérifiée.}

	\emph{Ceci permet aussi de comprendre pourquoi l'équation
	$ v_\textrm{\tiny\emph{app}} = \frac{v}{1 + v/c} $ est toujours vraie (rapprochement ou éloignement)
	si
	$v$ est la vitesse \emph{instantanée} (\emph{\/i.e.} définie comme
	comme une valeur limite en $t_0 = t$ quand $\Delta{}t \rightarrow 0$) :
	il y a toujours un $ \epsilon > 0 $ pour lesquels la condition
	ci-dessus est vérifiée entre $ t - \epsilon $  et $ t + \epsilon $ sauf, justement,
	lors du demi-tour.}

\end{quote}

\section{Une différence entre l'aller et le retour ?}

Le docteur Lengrand remarque une chose qui peut surprendre : la dernière relation
ci-dessus est vraie à l'aller et fausse au retour si on prend les valeurs de
coordonnées calculées par la Relativité Restreinte. Il en conclut qu'au retour elle
devrait être vraie aussi, et, donc, que lors du retour la vers la Terre l'espace s'est
dilaté au point que la Terre est beaucoup plus lointaine.

L'explication est toute autre\;! Si la Terre émet deux signaux vers le voyageur
le premier reçu au départ, le second à l'arrivée, entre ces événements les deux jumeaux
sont en mouvement relatif uniforme à vitesse constante $v$.

En revanche, lors du retour, si la Terre émet deux signaux vers le voyageur
le premier reçu quand il commence sont retour et le second quand il est
arrivé sur Terre, le premier signal a été émis, lui, \emph{avant}
le demi-tour, entre ces deux émissions leur vitesse relative change :
elle passe de $v$ à $-v$.

Le même problème se pose en prenant un jumeau qui part de l'étoile lointaine à $t=0$,
donc avec une vitesse nulle quand $ t < 0 $. Ce qui se comprend bien, puisque
si il était parti \emph{avant} pour être au lieu de l'étoile à $t = 0$ il aurait
fallu qu'il soit alors \emph{plus loin} (où que la Terre soit \emph{plus loin}, ça
revient au même) pour recevoir le signal. C'est ça la valeur dilatée de distance, qu'il appelle
\emph{« effet zoom »}, ce n'est pas la distance entre la Terre et le voyageur au
début du voyage de retour\;!

Pour résumer : le docteur Lengrand pose une formule qui est correcte pour un type de
trajectoire, mais comme il prend soin d'omettre tout démonstration il ne vérifie pas
les conditions de son application et l'applique à une situation qui ne vérifie pas
ces conditions\;!

La condition : 

\begin{quote}
	\emph{%
	Entre les deux événements considérés pour évaluer la vitesse apparente, les deux
	jumeaux sont en déplacement uniforme à vitesse $v$ l'un par rapport à l'autre,
	en particulier les événements de \emph{réception} des signaux.
	}
\end{quote}

\noindent n'étant pas vérifiée, \emph{au retour}, il n'y pas de raison que :

$$ \textrm{distance parcourue} = \textrm{vitesse apparente} \times \textrm{temps propre écoulé} $$

\begin{quote}
\emph{Ça n'a même, au fond, pas grand chose à voir avec la Relativité, c'est
le cas pour n'importe quelle situation où un observateur s'approche d'un
truc (ou un truc s'approche de l'observateur, ce n'est qu'une question
de point de vue), et que l'on prend deux signaux comme références : l'un reçu au départ, l'autre
à l'arrivée\;; la trajectoire n'étant pas uniforme avec la même vitesse hors de ce segment\;;
	la relation $ \Delta{}x = v_{\textrm{\tiny\emph{app}}} \times \Delta{}t $ est \emph{fausse}.
En revanche elle est vraie si l'observateur et le truc s'éloignent l'un de l'autre, parce
	qu'il reste vrai que $ x(t) = D + vt $ de bout en bout.
	}
\end{quote}

\begin{center}
  \def\svgwidth{0.75\linewidth}
  {
  \input{lengrouille.pdf_tex}
	}
\end{center}

\begin{center}
  \def\svgwidth{0.75\linewidth}
  {
  \input{lengrouille2.pdf_tex}
	}
\end{center}

\begin{center}
  \def\svgwidth{\linewidth}
  {\small
  \input{lengruche.pdf_tex}
	}
\end{center}
	
\section{Ceci explique sans doute cela...}

On peut se demander pourquoi, alors qu'il lui arrive, rarement certes, de
justifier ses formules, le docteur Lengrand ne le fait pas
pour $v_{\textrm{\tiny app}}$, pour une fois qu'il en a une de bonne\;?

Mon hypothèse est que le docteur Lengrand est parfaitement conscient que pour établir cette formule il 
faut passer par les valeurs de temps $t$, $t_0$ et $t_1$ qui n'ont de définitions que
si l'on dispose d'horloges synchronisées selon la procédure d'Einstein-Poincaré. 

Or il proclame régulièrement que cette procédure est inconsistente (sans fournir le moindre
argument, alors qu'on peut démontrer qu'elle est consistente sous un minimum d'hypothèses
que, par ailleurs, il ne conteste pas).

Dès lors il ne peut pas \emph{rédiger} (en supposant qu'il en soit capable)
la démonstration sans se \emph{dédire}.

On parle sur les forums scientifiques d'\emph{arrognorance} pour décrire la combinaison
de l'arrogance et de l'ignorance. On a ici, en plus, une forme de \emph{stupypocrisie}
c'est-à-dire de suffisance malhonnête empreinte de stupidité.

\end{document}
