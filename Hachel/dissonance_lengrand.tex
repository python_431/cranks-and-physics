\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{geometry}
\usepackage{fourier}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage[french]{babel}
\usepackage{url}
\usepackage{graphicx}
\usepackage{color}

\usepackage[%
    all,
    defaultlines=3 % nombre minimum de lignes
]{nowidow}


\parskip12pt
%\parindent0pt

\title{
Un cas intéressant de blocage mental et de dissonance cognitive :\\
Procédure de synchronisation d'Einstein-Poincaré et docteur Lengrand}
    
\author{Jean-Pierre \textsc{Messager} }

%\date{\today} 
\date{Version en cours d'édition}
% \date{\today} date coulde be today 
% \date{25.12.00} or be a certain date
% \date{ } or there is no date 
\begin{document}
% Hint: \title{what ever}, \author{who care} and \date{when ever} could stand 
% before or after the \begin{document} command 
% BUT the \maketitle command MUST come AFTER the \begin{document} command! 
\maketitle

\begin{flushright}
\begin{minipage}{9cm}
\emph{La folie, c'est de faire toujours la même chose et de s'attendre à un résultat différent.}
\end{minipage}\\
--- Albert \textsc{Einstein}
\end{flushright}

Ce qui formidable avec certains cranks\footnote{%
Pour une définition du terme \emph{crank}, se référer à 
l'article \emph{Einstein et les divagations d'un médecin de
campagne}, J.-P. \textsc{Messager}, 2023 :
\url{https://gitlab.com/python_431/cranks-and-physics/-/blob/main/Hachel/divagation_lengrand.pdf}
} c'est que lorsque l'on croit
qu'on aurait tout lu comme absurdités ils arrivent à en produire
de pire encore. Et leur dissonances cognitives et leurs faculté
à sortir des trucs délirants d'on ne sait où en surplus d'une
procédure technique parfaitement définie (on avait vu aussi avec
le GPS où Hachel/Lengrand invente tout un tas de fantaisies comme des
horloges atomiques dans les récepteurs où une synchronisation sur
une horloge infiniment lointaine dans une quatrième dimension
spatiale...)

Il s'agit ici d'un compte-rendu d'échanges sur la procédure de
synchronisation décrite par Einstein dans son article de 1905,
discussions qui ont déjà eu lieu il y 17 ans, et plus récemment
sur les forums \emph{sci.physics.relativity} et
\emph{fr.sci.physique}.

\noindent {\small \url{https://groups.google.com/g/fr.sci.physique/c/KgqI9gqTkR8/m/oMc9X0XjCWMJ}}

Rappels sur la procédure\footnote{%
Telle que décrite dans l'article \emph{On the Electrodynamics of
Moving Bodies}, A. \textsc{Einstein}, 1905 :\\
\url{https://www.fourmilab.ch/etexts/einstein/specrel/www/}
} : deux horloges A et B en tout point
identiques sont immobiles l'une par rapport à l'autre à une certaine
distance. Leur identité de fonctionnement (dans les limites des
précisions de mesures) permet de supposer qu'elle «~battent à la
même vitesse~»). RIEN de plus n'est supposé, en particulier rien
sur la valeur qu'elles indiquent, le but étant JUSTEMENT d'ajuster
l'une de ces horloges en lui appliquant un décalage après un calcul
impliquant des valeurs indiquées sur ces horloges lors d'événement
bien précis, événements qui se produisent AU LIEU DE CHAQUE HORLOGE.

La procédure décrite par Einstein n'est pas à la lettre une procédure
de synchronisation mais une procédure de VÉRIFICATION de leur
synchronisation. C'est la principale différence avec l'approche de
Poincaré. Cependant on peut prouver que la procédure de Poincaré
mène à des horloges synchronisées au sens d'Einstein. On peut aussi
transformer la procédure de vérification d'Einstein en une procédure
de synchronisation car elle permet de calculer le décalage à appliquer
à l'une ou l'autre des horloges (au choix).

Étapes de la méthode d'Einstein :

\begin{itemize}
\item[--] Lorsque l'horloge A indique $t_A$ un signal lumineux est émis par A en direction de B.
\item[--] Lorsque ce signal est reçu en B, l'horloge B indique $t_B$, un signal lumineux est émis par B en direction de A.
\item[--] Lorsque le signal est reçu en A, l'horloge A indique $t'_A$.
\end{itemize}

Les valeurs $t_A$, $t_B$ et $t'_A$ concernent des événements qui se produisent
tous aux lieux exacts de l'horloge qui indique ces mesures. Elle sont
parfaitement objectives et indépendantes de tout observateur. N'importe
où dans l'Univers, en A, en B, où sur Andromède des observateurs peuvent
les obtenir (par pigeons voyageurs cosmonautes par exemple).

Hachel/Lengrand réussit à nier ce simple FAIT. Premier niveau de
dissonance cognitive grave : des \emph{grandeurs} univoques dont
les \emph{valeurs} dépendraient de l'observateur !

Einstein fait remarquer que l'expérience (des mesures de temps lors
de trajets aller-retour, donc avec une seule horloge impliquée)
justifie la formule :

$$
   \frac{2(AB)}{t'_A - t_A} = c \mbox{~~~} (*)
$$

Il pose ensuite une \emph{convention} :

$$
   t_B - t_A = t'_A - t_B \mbox{~~~} (**)
$$

Face à cette équation Hachel/Lengrand considère que ce n'est possible que si les horloges
ont été spécialement réglée à l'avance, il n'y a rien de tel dans la
procédure d'Einstein. Le but est \emph{justement} de vérifier si cette
formule est vérifiée ou pas. Et si elle ne l'est pas de trouver une
façon de faire en sorte qu'elle le soit.

On voit là la capacité de Hachel/Lengrand à sortir des conditions
supplémentaires de nulle part (pour rester poli) et de là partir
complètement à côté de la plaque avec des valeurs objectives
qui n'ont pas la même valeur pour tout le monde, et en comparant
avec un scénario style jumeaux de Langevin complètement hors sujet...

\section*{Mais alors... Que faire si (**) est fausse ?}

Ou comment transformer une procédure de \emph{vérification
de synchronisation} en procédure de \emph{synchronisation}.
Gardons en tête que nous avons tout le \emph{temps} que nous
voulons, avec n'importe quels moyens, pour communiquer
$t_A$ et $t'_A$ à B, ou bien $t_B$ à A.

En partant de :

%\begin{math}
\begin{eqnarray}
%{lllr}
\nonumber \frac{2(AB)}{t'_A - t_A} &=& c \mbox{~~~~~~~~~~~~~~} (*)\\
\nonumber t_B - t_A &=& t'_A - t_B \mbox{~~~~} (**)
\end{eqnarray}
%\end{math}


L'algèbre élémentaire permet d'exprimer $t_A$ en fonction des
autres grandeurs impliquées :

\begin{eqnarray}
\nonumber t'_A &=& t_A + \frac{2(AB)}{c} \\ 
\nonumber t'_A &=& 2t_B - t_A\\ 
\nonumber t_A + \frac{2(AB)}{c} &=& 2t_B - t_A \\ 
\nonumber 2t_A &=& 2\left( t_B - \frac{(AB)}{c} \right) \\ 
\nonumber t_A &=& t_B - \frac{(AB)}{c} \\
\nonumber t_A &=& t_B - \frac{t'_A - t_A}{2}
\end{eqnarray}

La valeur de $t_A$ qu'on aurait du avoir est
$ t_B - \frac{(AB)}{c} = t_B - \frac{t'_A - t_A}{2}$. Notons
qu'il n'est même pas nécessaire de connaître la distance $AB$~~!
Mieux encore : la méthode permet de la \emph{calculer}~~!
De même, un autre signal que la lumière pourrait être utilisé
dès lors qu'il se déplace à vitesse constante par rapport à
sa source. Un canon à petits pois par exemple.

Si la condition (**) n'est pas vérifiée, alors on décale l'horloge A de :

$$ t_B - \frac{t'_A - t_A}{2} - t_A = \frac{3t_A - t_A' +2t_B}{2} $$

On pourrait, alternativement, calculer
le $t_B$ attendu et décaler l'horloge B de la valeur opposée :

$$ t_A + \frac{t'_A - t_A}{2} - t_B = \frac{t'_A + t_A -2t_B}{2} $$

\noindent ça revient strictement au même\footnote{%
J'ai mis en ligne une application Web montrant toute
la procédure en action : \url{https://www.noedge.net/e/}}.
L'un ou l'autre décalage ayant été appliqué on peut relancer
la procédure de vérification (dans un sens comme dans l'autre)
et vérifier que horloges seront, alors,
\emph{synchronisées} selon le critère (**).

La procédure fonctionne quels que soient les réglages
initiaux des deux horloges. On peut appeler alors la
relation (**) vérifiée par les deux horloges de «~A
est synchronisé avec B~» ou encore «~A synch B~».

Pour quelle soit consistante il reste à vérifier que «~synch~»
vérifie bien ces conditions
(sous l'hypothèse $\frac{2(AB)}{t'_A - t_A} = c$, que Hachel/Lengrand
considère vraie~~!) :

A synch A (réflexivité)

A synch B $\Longrightarrow$ B synch A (symétrie)

(A synch B) et (B synch C) $\Longrightarrow$ A synch C (transivité)

Einstein juge inutile de le faire dans son article, le jugeant
évident au yeux de son lectorat (il n'était pas là pour gérer
à l'avance les cranks).

La procédure est aussi expérimentalement falsifiable, malgré
son aspect conventionnel : en testant à nouveau la synchronisation
après une minute, une heure, une année ou un siècle pour les
même horloges, laissées à leur cours, on constaterait une
désynchronisation due un phénomène quelconque (sauf si défaut technique
des horloges), ce qui donnerait du sens à l'expression trop souvent lue
dans des ouvrages de vulgarisation : «~le temps s'écoule plus ou moins
vite là ou là~». D'innombrables expériences valident cet aspect de la
procédure d'Einstein.

Cette procédure donne un sens à la coordonnée $t$ d'un événement pour
un référentiel inertiel quelconque (donc $t'$, $t''$, etc.)

En RG on retrouve cette procédure avec une limitation : elle est
purement locale : elle vaut au voisinage spatio-temporel d'un
événement. Et il faut tenir compte du fait que, par définition
de la Gravitation, deux corps en mouvements libres (pas de forces
agissantes) peuvent voir leur trajectoires s'éloigner ou se rapprocher.

Cette subtilité permet d'éclairer un aspect circulaire de la physique
(ce qui est tout à fait normal, et plutôt bon signe) : les horloges
sont réglées pour rendre vrai le premier principe de Newton et le
premier principe de Newton permet de régler les horloges de façon
cohérente (localement).

Ce n'est d'ailleurs pas un hasard si l'étiquetage temporel des
événements en \emph{«~direct live~»} i.e. ce que propose Hachel/Lengrand
est incohérent dans ce sens : avec de telles coordonnées le
premier principe de Newton est violé systématiquement, au pire
on obtient même une vitesse \emph{«~apparente~»}) non seulement variable
mais \emph{discontinue}\footnote{%
J'ai écrit un petit programme en Python qui montre ce phénomène
graphiquement :
\url{https://gitlab.com/python_431/cranks-and-physics/-/tree/main/Hachel/code}}
(si la trajectoire du corps croise l'observateur).

Ce que Hachel/Lengrand présente comme une impossibilité de synchronisation
n'est que la validation que la procédure qu'il propose est incohérente.
C'est-à-dire qu'un critère de synchronisation
du style «~en A je marque $t_A$ et je \emph{vois}, de A, qu'en même temps B marque $t_B$,
et on a $t_A = t_B$~» est un critère inconsistent.

Au contraire la méthode proposée par Einstein, ou son équivalent par
Poincaré, est cohérente.

\section*{Une réponse de Lengrand totalement inepte}

Le 19 août 2024, Lengrand a répondu en annotant une version
précédente de cet article :

\begin{center}
\includegraphics[width=\linewidth]{hachelsyncans1.png}
\end{center}

Dans les deux cas le critère de «~dépendance~» de l'observateur, ou
de l'«~examinateur~» \emph{(sic)}, est ABSURDE. Il y a TROIS grandeurs
totalement univoques qui ne dépendent d'aucun point de vue. C'est
ce que marquent les horloges A et B. POINT. Ça pourrait être des
thermomètres, des hydromètres, des horloges ou des tensiomètres
ce serait pareil. Comment ce type peut avoir été un jour médecin,
diantre !


Et pour ce qui est de la seconde équation (**) c'est une \emph{définition}
du critère de synchronicité. C'est faux ou non selon le réglage
initial des horloges, ce qui compte c'est que :

\begin{itemize}
\item[--] ça reste vrai dans la suite du fonctionnement des deux horloges.
Et c'est le cas.
\item[--] ça soit cohérent (réflexif, symétrique, transitif). Et ça l'est.
\end{itemize}

Ça commence mal... Lengrand retombe dans la dissonance cognitive,
en prétendant que 3 pourrait valoir 4 selon l'observateur.
C'est hallucinant !

\emph{«~Une hirondelle étant une hirondelle~»} :
Non, c'est la conséquence de (*) et si c'est vrai pour la lumière
pour tout référentiel inertiel, c'est faux pour d'autres types de
signaux comme le son (par exemple).

(En passant) La première annotation en rouge ne vaut pas mieux : si
on comprend que par les valeurs $t_A, t_B$ et $t'_B$ Lengrand entend les
instants des évènements d'émis\-sions/récep\-tion pour un observateur
donné (et non pas les très prosaïques valeurs inéquivoquement indiquées
par les horloges placées en ces lieux), ce qu'il a confirmé, alors
l'équation $\frac{2(AB)}{(t'_A - t_A)}$ est en réalité \emph{fausse}
pour un référentiel ou les horloges ne sont pas au repos~~! Ceci
tout autant en Relativité Galiléenne que moderne.

\emph{«~??? Et voilà comment, à la va-vite, on fusille la théorie
de la relativité restreinte telle qu'il faudrait la comprendre.~»} :
La suite c'est de l'algèbre de collège, et pourtant, commentaire
\emph{«~???~»}... Misère.

Bref, après trente ans Lengrand est bel et bien toujours l'ahuri
imbécile mythomane qui ne comprend RIEN à RIEN et n'arrive pas à
progresser d'un pouce.

En 2007, l'équation $t_B - t_A = t'_A - t_B$ lui inspirait ce
commentaire (il faut se frotter les yeux pour le croire) :

\begin{quote}
\emph{?????\\
Attends, je rêve, là...\\
Cela veut dire qu'Einstein trouve que les montres sont synchronisées si elles battent à la même vitesse???\\
C'est ça que tu veux dire???\\
\textbf{Parce que l'équation dite ici dessus, c'est ça.} \emph{[souligné par nous]}\\
Mais j'en ai rien à foutre de ça! Je le sais implicitement, ça! N'importe quel abruti (même Vicnent t'as qu'à voir) le sait implicitement!\\
Mais c'est PAS DU TOUT mon propos. J'en parle même pas de ça. C'est tellement évident que je n'en parle pas.}
\end{quote}

Y a-t-il eu progrès~? Le lecteur sera juge\footnote{%
Confronté à ses propres propos d'alors, Lengrand s'est défaussé en
affirmant avoir « oublié » un point d'interrogation à la fin de
la phrase ici en gras. Ce qui a fait rigoler des marmottes et
mon cheval.
}...

Plus récemment, sur le forum anglophone \emph{sci.physics.relativity},
Lengrand précise ce qu'il entendait (j'ai corrigé les notations
incorrecte au passage) :

\begin{quote}
\emph{In short, for A, $t_B$ and $t'_A$ are simultaneous.}
\end{quote}

Ce qui n'a, en soit, pas grand sens : deux \emph{nombres} ne
peuvent pas être «~simultanés~» ou non... Soyons indulgent, il
est probable qu'il voulait dire que \emph{Les événements e1 :
«~B émets un signal vers A~» et e2 : «~A reçoit le signal émis
par B~» sont simultanés pour A.}

Or, d'une part, à ce stade la simultanéité en est au stade de
la définition, justement et, d'autre part, avec cette définition
c'est faux (de plus l'expression \emph{«~pour A~»} y est dénué
de sens).

Que l'on accepte ou non la définition d'Einstein de la simultanéité
on ne peut pas nier que cette définition \emph{implique} que \emph{e1} et
\emph{e2} ne sont pas simultanés !

La définition que Lengrand préfère repose sur la réception
d'un signal lumineux pour un événement distant. Encore une fois,
pourquoi pas~? --- mais on a vu plus haut les défauts rédhibitoires d'une
telle convention\footnote{%
... qui par ailleurs est connue dans la littérature sous le terme
de \emph{synchronisation du radar}.
}: inconsistence (donc impossibilité de définir un
référentiel), incompatibilité avec la première loi de Newton, etc.

Lengrand est tout bonnement incapable de rester concentré quelques
minutes et d'examiner les propriétés et conséquences d'une convention
sans introduire des fantaisies issues de ses préjugés. Ceci rend quasi
impossible toute discussion sensée sur le fond, et lui bloque à tout
jamais la compréhension de la Relativité.

\newpage

\section*{Épilogue}

Le 4 septembre 2024, pour ainsi dire \emph{au même moment où je
mettai en ligne mon application} (ironie de l'histoire...) Lengrand
posta ceci sur \emph{fr.sci.physique} (et une version dans un 
anglais presque acceptable sur \emph{sci.physics.relativity}) :

\begin{quote}
	\emph{\textbf{Philosophie relativiste et synchronisation}\\
	Le problème de la relativité, c'est la compréhension de la notion de temps présent, c'est à dire la notion de simultanéité (qu'il ne faut pas confondre avec la notion de chronotropie). Y a-t-il sur la planète Fomalhaut IV, une princesse Alexandra qui vit là-bas, en même temps que moi ;  moi qui me trouve ici sur terre?\\
C'est à dire dans le même instant présent? Il faut bien dire que oui, puisque quelque soit la procédure de synchronisation universelle que j'adopte, soit la mienne, soit celle d'Albert Einstein, il y a forcément une ETIQUETTE, et une seule, pour caractériser l'existence d'Alexandra simultanée avec  la mienne. Mais selon la méthode de "synchronisation du temps présent", nous n'aurons pas la même étiquette. Einstein utilise la procédure M, Hachel la procédure H. La procédure M est la plus pratique, la procédure H est la plus vraie.\\
La procédure M est la plus pratique, car elle dérive de la synchronisation du temps présent sur un point M placé très loin dans
une quatrième dimension imaginaire, et à égale distance de tous les points constituant notre univers.  Cela donne un temps universel abstrait, mais fort utile, où la notion de temps présent universel est plat, et réciproque. Si A existe en même temps que B pour M, alors B existe en même temps que A pour M. c'est très pratique.\\
	La procédure H proposée par Richard Hachel est moins pratique, mais plus vraie. Elle est moins pratique, car la notion de symétrie du temps présent n'y sera pas absolue. Mais elle est plus vraie, physiquement plus juste, et plus belle. Elle demeurera éternellement vraie expérimentalement, et éternellement plus belle philosophiquement. Quoi de plus beau que de dire à un enfant :"Ce cheval dans ce pré, cette lune dans le ciel, cette galaxie dans ce télescope, tu les vois instantanément, tels qu'ils sont aujourd'hui, en direct-live". Quoi de plus laid que la pensée humaine, qui se croit intelligente,
	bien qu'elle soit remplie de railleries à la con, d'imbécillités conceptuelles, tout simplement parce qu'elle peut dire, comme disent tous les crétins : "La vitesse de la lumière, c'est c, nous le savons, nous l'avons mesurée, expérimentée, et nous obtenons $3.10^{8}$m/s".
	Il s'agit là de la réflexion la plus débile de l'histoire de l'humanité, proposée par des crétins railleurs (\mbox{Python} \emph{[pseudonyme de l'auteur de cet article]}, John Baez) qui se croient drôle et intelligents, railleurs autorisés, mais qui n'ont rien compris à la notion d'anisochronie universelle et aux deux façons possibles dont on peut (voire on DOIT pouvoir) synchroniser les horloges de l'univers.
	}
\end{quote}

Un triste portrait du docteur \textsc{Lengrand} ne vous vient-il pas
en tête~~?

\begin{center}
	\includegraphics[width=0.6\linewidth]{560pxGuignolinLyon1.jpg}\\
	(crédit : \emph{Wikimedia})
\end{center}

\end{document}
