#!/usr/bin/env python3

from math import sin, cos
D = 1.0

defs = f"D = {D}"

def x(v, t: float) -> float:
    """x(t) = D - v*t"""
    #return D - v*t
    return v*t

def y(v, t: float) -> float:
    """y(t) = 0.3"""
    #return 0.3
    return v*t/2 

