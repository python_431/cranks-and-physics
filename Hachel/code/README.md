# Affichage du graphe des vitesses et vitesses apparentes

Définir les equations du mouvement (x et y seulement) dans `motion.py`, par exemple :

~~~~Python
D = 1.0

def x(v, t: float) -> float:
    return D - v*t

def y(v, t: float) -> float:
    return 0.3
~~~~

Exécuter `trajectory.py` pour visualiser le graphe des vitesse apparentes :

~~~~Bash
$ python3 trajectory.py
~~~~

![](app_speeds.png)
