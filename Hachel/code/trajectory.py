#!/usr/bin/env python3

from motion import x, y, defs
from matplotlib import pyplot as plt
from math import sqrt

TMIN = -5
TMAX =  10 
epsilon = 0.1

v = 0.3
c = 1

def iterate_time():
    t = TMIN
    while True:
        if t > TMAX:
            return None
        yield t
        t += epsilon

def trajectory():
    return [ (t, x(v, t), y(v, t)) for t in iterate_time() ]

def spatial_part(traj):
    return [ (x,y) for (_, x, y) in traj ]

def dist(x0, y0, x1, y1):
    return sqrt( (x1-x0)**2 + (y1-y0)**2 )

def apparent_time(t, x, y):
    #d = sqrt(x**x + y**y)
    d = dist(0, 0, x, y)
    return t - d/c

def real_speeds(traj):
    segments = zip(traj, traj[1:])
    return [ ((t1+t0)/2, dist(x0, y0, x1, y1)/(t1-t0)) for (t0, x0, y0), (t1, x1, y1) in segments ]

def apparent_speeds(traj):
    segments = zip(traj, traj[1:])
    return [ ((t1+t0)/2, dist(x1, y1, x0, y0)/(apparent_time(t1, x1, y1)-apparent_time(t0, x0, y0))) \
            for (t0, x0, y0), (t1, x1, y1) in segments ]

def draw_trajectory():
    graph = spatial_part(trajectory())
    plt.plot(*zip(*graph))
    return plt


def draw_apparent_trajectory():
    graph = spatial_part(trajectory())
    plt.plot(*zip(*graph))
    return plt

def draw_real_speeds():
    graph = real_speeds(trajectory())
    fig, ax = plt.subplots()
    ax.set_title(f"Vitesse\n{defs} v = {v} ; {x.__doc__} ; {y.__doc__}")
    ax.set_ylabel("V")
    ax.set_xlabel("t")
    ax.plot(*zip(*graph))
    return plt

def draw_apparent_speeds():
    graph = apparent_speeds(trajectory())
    fig, ax = plt.subplots()
    ax.set_title(f"Vitesse apparente\n{defs} v = {v} ; {x.__doc__} ; {y.__doc__}")
    ax.set_ylabel("V_app")
    ax.set_xlabel("t")
    ax.plot(*zip(*graph))
    return plt

#print(spatial_part(trajectory()))
#draw_trajectory().show()
plot = draw_apparent_speeds()
plot.savefig("app_speeds.png")
plot.show()

